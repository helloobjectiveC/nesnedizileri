//
//  main.m
//  diziNesneleri
//
//  Created by Yakup on 17.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

/*
 
 Dizi nesneleri iki kısma ayrılır.
 1- mutable --> Değiştirilebilir - NSMutableArray Sınıfını kullanır
 2- immutable --> Sabit - NSArray sınıfını kullanır
 3-sortedArrayUsingSelector metotlarıyla sırala yapılabilir.
 */


#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Sabir Bir Array Dizisi Oluşturma
        NSArray *aylar;
        aylar = [NSArray arrayWithObjects: @"Ocak",@"Şubat",@"Mart",@"Nisan",nil];
        
        //Değiştirilebilir Bir Array Dizisi Oluşturma.
        NSMutableArray *rakamlar;
        rakamlar = [NSMutableArray arrayWithObjects:@"1",@"3",@"5",@"7",@"9",nil];
        
        // Dizi elaman sayısı bulma.
        unsigned long diziElamanSayisi = [rakamlar count];
        NSLog(@"Rakamlar Dizisi Elman Sayisi: %lu",diziElamanSayisi);
        printf("--------------------------------\n");
        // For döngüsü ile dizi elamanlarına erişme.
        for (int i = 0; i<diziElamanSayisi; i++) {
            NSLog(@"Dizi %i. Elamanı = %@",(i+1),[rakamlar objectAtIndex:i]);
        }
         printf("--------------------------------\n");
        //Değiştirilebilir diziye bir elaman ekleme.
        [rakamlar addObject: @11];
        for (int i = 0; i<diziElamanSayisi; i++) {
            NSLog(@"Dizi %i. Elamanı = %@",(i+1),[rakamlar objectAtIndex:i]);
        }
        printf("--------------------------------\n");
        //Araya Bir Elaman Eklemme
        [rakamlar insertObject:@4 atIndex:2];
        for (int i = 0; i<diziElamanSayisi; i++) {
            NSLog(@"Dizi %i. Elamanı = %@",(i+1),[rakamlar objectAtIndex:i]);
        }
        printf("--------------------------------\n");
        //Diziden elaman silme
        [rakamlar removeObjectAtIndex:2];
        for (int i = 0; i<diziElamanSayisi; i++) {
            NSLog(@"Dizi %i. Elamanı = %@",(i+1),[rakamlar objectAtIndex:i]);
        }
        
    }
    return 0;
}
